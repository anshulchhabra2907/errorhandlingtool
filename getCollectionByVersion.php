<?php
/**
 * Created by PhpStorm.
 * User: Anshul
 * Date: 13/4/17
 * Time: 3:35 PM
 */
require('master_connection_elasticsearch.php');

$es = new ElasticSearch();
$conn = $es->getEsConnection();

if(isset($_REQUEST["d"])){
    $date = $_REQUEST["d"];
    $dateArray=explode("_",$date);

    if((count($dateArray) != 3) || (strlen($dateArray[0])!= 2) ||strlen($dateArray[1])!= 2 ||strlen($dateArray[2])!= 4){
        echo "Please enter date in dd_mm_yyyy format";
        exit;
    }
    //changed for elastic search in hurry by Anshul
    $date = date_format($date,"Y.m.d");
}else{
    $date =  date('Y.m.d',strtotime("yesterday"));
//$date =  date('Y-m-d',strtotime("today"));
//$date =  explode("-",date('Y-m-d',strtotime("tomorrow")));
}
if(isset($_REQUEST["bo_vc"])){
    $bo_vc = $_REQUEST["bo_vc"];

    if($bo_vc < 100000){
        echo "Please enter bo_vc in valid range";
        exit;
    }
} else{
    $bo_vc = 101712;
}

//$collName = "error_data_sdk_".$date;
//$conn->setCollection($collName);
//$filter = [];
//$list = $conn->find(array("d.bo_vc" => array('$gte' => $bo_vc)),array('limit' => 0));

$params = [
    'index' => 'filebeat-'.$date,
    'type' => 'nginx_access',
    'body' => [
        "fields"=> ["message"],
        'query' => [
            'match' => [
                "end_point"=> "/v2/report/error"
            ]
        ]
    ]
];
$response = $conn->search($params);
$exceptionDataArray = $response["hits"]["hits"];

$exceptionSortedArray = array();
$osVersionSortedArray = array();
$pIdSortedArray = array();
$appVersionSortedArray = array();
$handsetSortedArray = array();
$sdkBuildVersionSortedArray = array();
$handsetWithHandsetMakeSortedArray = array();
$deviceIdSortedArray = array();

$finalSortedArray=array();
$total=0;


if(count($exceptionDataArray)>0){
    foreach ($exceptionDataArray as $data)
    {
        $jsonString = $data["fields"]["message"][0];
        if (strpos($jsonString, '{\x22') !== false) {
            $doc = json_decode(getJsonString($jsonString),true);
            if($doc['d']['bo_v'] == $bo_vc) {
                $total = $total + 1;
                $exceptionSortedArray[$doc['e']['Exception_Class_Name']][] = $doc;
                $osVersionSortedArray[$doc['d']['osv']][] = $doc;
                $pIdSortedArray[$doc['pid']][] = $doc;
                $appVersionSortedArray[$doc['d']['App_Version']][] = $doc;
                $handsetSortedArray[$doc['d']['hm']][] = $doc;
                $sdkBuildVersionSortedArray[$doc['d']['bo_v']][] = $doc;
                $deviceIdSortedArray[$doc['d']['did']][] = $doc;
            }
        }
    }
}else{
    echo "no data found!!";
}
$collName = "";


$exceptionSortedAsCountArray['exception'] = sortAsCountArray($exceptionSortedArray,$collName,"e.Exception_Class_Name");
$osVersionSortedAsCountArray['os_version'] = sortAsCountArray($osVersionSortedArray,$collName,"d.osv");
$pIdSortedArrayAsCountArray['pid'] = sortAsCountArray($pIdSortedArray,$collName,"pid");
$appVersionSortedAsCountArray['app_version'] = sortAsCountArray($appVersionSortedArray,$collName,"d.App_Version");
$handsetSortedAsCountArray['handset'] = sortAsCountArray($handsetSortedArray,$collName,"d.hm");
$sdkBuildVersionSortedAsCountArray['sdkBuildVersion'] = sortAsCountArray($sdkBuildVersionSortedArray,$collName,"d.bo_v");

$finalSortedArray =
    array_merge(array("total"=>$total),$exceptionSortedAsCountArray,$osVersionSortedAsCountArray
        ,$pIdSortedArrayAsCountArray,$appVersionSortedAsCountArray
        ,$handsetSortedAsCountArray, $sdkBuildVersionSortedAsCountArray);

echo json_encode($finalSortedArray);

$headers = "From:SDK_EXCEPTION_REPORT <betaout.errors@gmail.com>\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=utf-8\r\n";
$headers .= "Content-Transfer-Encoding:base64 \r\n";
$messagebody= $base64contents = rtrim(chunk_split(base64_encode(json_encode($finalSortedArray))));
mail("betaout.errors@gmail.com,anshul@betaoutinc.com,jayant@betaoutinc.com,shubham@betaoutinc.com", "SDK_EXCEPTION_REPORT VERSION: .$bo_vc . DATE:".$date, $messagebody, $headers,"-fsender@example.com");


function sortAsCountArray($toSortAsCountArray,$cName,$filterId){
    $finalArray = array();
    $i=0;
    foreach ($toSortAsCountArray as $k=>$v)
    {
        $finalArray[$i]['id']=$k;
        $finalArray[$i]['c']=count($toSortAsCountArray[$k]);
        $filter = array($filterId=>$k);
        $data = array("c_name"=>$cName,"filter"=>$filter);
        $finalArray[$i]['qUrl'] = "https://stag.betaout.in/jsodis?data=".json_encode($data);
        $i+=1;
    }
    return $finalArray;
}

function getJsonString($jsonString){
    $newJsonWithRemovedHeader = strstr($jsonString, '{\x22');

    $finalJsonString =  preg_replace_callback(
        "(\\\\x([0-9a-f]{2}))i",
        function ($a) {
            return chr(hexdec($a[1]));
        },
        $newJsonWithRemovedHeader
    );
    return $finalJsonString;
}

?>
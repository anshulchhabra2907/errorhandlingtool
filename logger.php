<?php

class Logger_Logger {

    static $_logString = '';

    public function log($arrLogMessage) {
        try {
            $logKeys = array('projectId', 'queuename', 'acktype', 'module', 'exetime', 'msgbytes', 'memconsumed', 'msgcount', 'bulkcount', 'bulkscriptcount', 'function', 'respcode', 'message');

            $gmtDateTime = gmdate("Y-m-d H:i:s", time());
            $gmtDateTime = str_replace(" ", "T", $gmtDateTime);
            $host = gethostname();
            $arrMsg = array("ltp" => "bo", "time" => $gmtDateTime, "host" => $host);
            foreach ($logKeys as $logKey) {
                if (isset($arrLogMessage[$logKey])) {
                    $arrMsg[$logKey] = $arrLogMessage[$logKey];
                }
            }
//            $logMessage = implode('!~~!', $arrMsg);
            $logMessage = json_encode($arrMsg);

            self::$_logString .= $logMessage . PHP_EOL;
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function writeLog() {
        try {
            if (strlen(self::$_logString) > 0) {
                error_log(self::$_logString, 3, '/var/log/consume.log');
                self::$_logString = '';
            }
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public static function exception($ex, $classFun) {
        try {
            global $debugMode;
            $arrLog['projectId'] = 0;
            $arrLog['module'] = 'Exception';
            $arrLog['function'] = $classFun;
            $arrLog['respcode'] = $ex->getCode();
            $arrLog['message'] = $ex->getMessage() . PHP_EOL . $ex->getTraceAsString();
            $objLogger = new Logger_Logger();
            $objLogger->log($arrLog);
            $objLogger->writeLog();
            if ($debugMode)
                print_r($arrLog);
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }
}

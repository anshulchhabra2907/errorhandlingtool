<?php
require 'vendor/autoload.php';
use Elasticsearch\ClientBuilder;

class ElasticSearch   {

    function getEsConnection(){
        $hosts = [
            'bo-bastion.southeastasia.cloudapp.azure.com:8101'        // SSL to localhost
        ];
        $client = ClientBuilder::create()->setHosts($hosts)->build();
        return $client;
    }
}

$date = "2017.07.07";



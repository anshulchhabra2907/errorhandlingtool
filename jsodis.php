<?php
/**
 * Created by PhpStorm.
 * User: Anshul
 * Date: 18/4/17
 * Time: 6:51 PM
 */

require('master_connection_mongo.php');

{
//    $doc = hash(base64_decode($_REQUEST["data"]));
    if (isset($_REQUEST["data"])) {
        $data = $_REQUEST["data"];
        echo json_encode($data);
        $data = preg_replace('/\\\"/', "\"", $data);
        echo json_encode($data);

        if (!empty(json_decode($data, true))) {
            $conn = getMongoConnection();
            $dataArray = json_decode($data, true);
            $conn->setCollection($dataArray['c_name']);

            $filterArrayJson = generateRegexFilterArray($dataArray['filter']);
//            $list = $coll->find($dataArray['filter']);
            $list = $conn->find($filterArrayJson,array('limit' => 0));
            $finalListArray = array();
            foreach ($list as $doc) {
                array_push($finalListArray, $doc);
//                echo json_encode($doc);
//                echo "<br><br><br>";
            }
            echo json_encode($finalListArray);
            } else {
            error_log("Received data is empty");
        }
    } else {
        error_log("invalid url received");
    }
}
function generateRegexFilterArray($toFilterArray){
    $filteredArray = array();
    foreach ($toFilterArray  as $key=>$value){
        array_push($filteredArray,array($key => new MongoDB\BSON\Regex('/'.$value.'/i')));
    }
    $finalArrayWithAndCondition = array('$and' => $filteredArray);
    return $finalArrayWithAndCondition;
}

function getMongoConnection()
{
    $conn = new MongoConnection();
    return $conn;
}

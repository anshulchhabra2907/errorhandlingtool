<?php
/**
 * Created by PhpStorm.
 * User: Anshul
 * Date: 4/12/2017
 * Time: 4:37 PM
 */

require('master_connection_mongo.php');
/**
 * todo
 * api url = reportMSDK.php?token=hash(md5("store_betaout_err_data"))
 */

//if($_SERVER['REMOTE_ADDR']== "52.187.33.149") {
////    if (isset($_REQUEST[hash(md5("store_betaout_err_data"))])) {
makeConnectionAndSaveToMongo();
////    }else{
////        error_log("token id not matched");
////    }
//}else{
//    error_log("WhiteListed Ip not matched");
//}


function makeConnectionAndSaveToMongo()
{
//    $doc = hash(base64_decode($_REQUEST["data"]));
    if (isset($_REQUEST["data"])) {
        $doc = $_REQUEST["data"];
        if (!empty(json_decode($doc, true))) {

            $conn = getMongoConnection();

            $dataArray = json_decode($doc,true);

            /*
             * dumping all document in "error_data_sdk" collection
             */
            $conn->setCollection("error_data_sdk_master");

            $timeStamp = $dataArray["ts"];
            $date = date('d_m_Y', $timeStamp);
            $dataArray["date"] = $date;

            insertIntoMongo($conn,$dataArray);

            /*
             * dumping documents date wise
             */
            if(isset($dataArray["ts"])) {
                $conn->setCollection("error_data_sdk_" . $date);
            }else{
                $conn->setCollection("error_data_sdk_raw");
            }
            insertIntoMongo($conn,$dataArray);
            echo $doc;
        } else {
            error_log("Received data is empty");
        }
    } else {
        error_log("invalid url received");
    }
}

function insertIntoMongo($conn,$dataArray){
   $result= $conn->insert($dataArray);
   echo json_encode($result);
}

function getMongoConnection()
{
    $conn = new MongoConnection();
    return $conn;
}


?>



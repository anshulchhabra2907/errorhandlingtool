<?php
/**
 * Created by PhpStorm.
 * User: Anshul
 * Date: 14/4/17
 * Time: 3:42 PM
 */
require('master_connection_elasticsearch.php');

if($_REQUEST["require"] == "data"){
    $requiredData  = getDataFromMongo();
    echo $requiredData;
    exit;
}

function getDataFromMongo(){
    $es = new ElasticSearch();
    $conn = $es->getEsConnection();

    $size = $_REQUEST["size"];
    $pId = $_REQUEST["p_id"];
//    $fromDate = $_REQUEST["for_date"];
//    $fromDateTimeStamp = strtotime($fromDate);
//    $toDate = $_REQUEST["to_date"];
//    $toDateTimeStamp = strtotime($toDate."23:59:59");

    $forDate = $_REQUEST["for_date"];
    error_log("for date is");

    if($forDate == ("")) {
        $forDate = date("Y.m.d");
    }

    $params = [
        'index' => 'filebeat-'.$forDate,
        'type' => 'nginx_access',
        'body' => [
            "fields"=> ["message"],
            "size"=> 10000,
            'query' => [
                'match' => [
                    "end_point"=> "/v3/report/error"
                ]
            ]
        ]
    ];
    $response = $conn->search($params);
    $exceptionDataArray = $response["hits"]["hits"];
    $docListArray = array();
    if(count($exceptionDataArray)>0){
        foreach ($exceptionDataArray as $data)
        {

            $jsonString = $data["fields"]["message"][0];
            if (strpos($jsonString, '{\x22') !== false) {
                $newJsonWithRemovedHeader = strstr($jsonString, '{\x22');

                $finalJsonString =  preg_replace_callback(
                    "(\\\\x([0-9a-f]{2}))i",
                    function ($a) {
                        return chr(hexdec($a[1]));
                    },
                    $newJsonWithRemovedHeader
                );
            $doc = json_decode($finalJsonString,true);
                $row=array($doc['t'],$doc['e']['Exception_Class_Name'],$doc['e']['ExceptionMessage'],$doc['e']['Localized_Message'],
                    $doc['pid'],$doc['ts'],$doc['d']['hm'],
                    $doc['d']['m'],$doc['d']['nt'],$doc['d']['os'],$doc['d']['osv'],
                    $doc['d']['App_Version'],$doc['d']['app_pkg'],
                    $doc['e']['Stack_Trace'],json_encode($doc)
                );
                array_push($docListArray,$row);
            }
        }
        return json_encode($docListArray);
    }else{
        echo "no response found!!";
    }


//    if($pId != "" && $fromDate !="" ){
//        $filter = array(
//            '$and' => array(
//                array('ts' => array('$gte' => $fromDateTimeStamp)),
//                array('ts' => array('$lte' => $toDateTimeStamp)),
//                array('pid' => $pId)
//            )
//        );
//    }else if($pId != ""){
//        $filter = array(
//            '$and' => array(
//                array('pid' => $pId),
//                array('ts' => array('$lte' => $toDateTimeStamp))
//            )
//        );
//    }else if($fromDate !=""){
//        $filter = array(
//            '$and' => array(
//                array('ts' => array('$gte' => $fromDateTimeStamp)),
//                array('ts' => array('$lte' => $toDateTimeStamp)),
//            )
//        );
//    }else{
//        $filter = array('ts' => array('$lte' => $toDateTimeStamp)
//        );
//    }

//    $list = $conn->find($filter,array('limit' => $size));
}

?>
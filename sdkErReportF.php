<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <title>SDK REPORT</title>
    <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css"/>
</head>
<body>

Project Id: <br><input id="p_id" type="text">
<br>
<br>
For Date:<br><input  id="for_date_id" type="date">
<br>
<!--<br>-->
<!--To Date:<br><input  id="to_date_id" type="date">-->
<!--<br>-->
<br>

<input id="submit" type="submit" onclick="getData()">

<br>
<br>
<br>

<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%"/>


</body>

<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
        getData();
    });

    function getData() {
        var pId = document.getElementById("p_id").value;
//        var toDate = document.getElementById("to_date_id").value;
        var forDate = document.getElementById("for_date_id").value;

        $.ajax({
            //change this to sdkErReportB.php to run on local
//            url: "sdkErReportB.php",
            url: "sdkErReportB",
            data:{
              "require":"data"
                ,"size":"100"
                ,"p_id":pId
                ,"for_date":forDate

            },
            async:false,
            success: function(rowDataJson){
                if(rowDataJson!=0){
                    rowDataArray = JSON.parse(rowDataJson);
                    displayData(rowDataArray);
                }else{
                    displayData("NO RECORD FOUND");
                }
            }});
    }

    function displayData(detailedData){
        $('#example').DataTable( {
            destroy: true,
            data: detailedData,
            dom: 'Bfrtip',
            buttons: [
                'copy'
                ,'csv'
                ,'excel'
                ,'pdf'
//                ,'print'
            ],
            columns: [
                { title: "TITLE" },
                { title: "Exception Class Name" },
                { title: "Exception Message" },
                { title: "Localized Message" },
                { title: "PROJECT ID" },
                { title: "TIMESTAMP" },
                { title: "Brand" },
                { title: "Model" },
                { title: "Network" },
                { title: "OS" },
                { title: "OSV" },
                { title: "App Version" },
                { title: "APP Package" },
                { title: "Stack Trace" },
                { title: "FULL JSON"}
            ]
        } );
    }




</script>
</html>